This project is used to conduct the positive deviance analyse for the MIX-ENABLE project (CORE Organic Cofund Call 2016/17). 

**MIX-ENABLE**: MIXEd livestock farming for improved sustaiNABiLity and robustnEss of organic livestock

# Configuration
Works with: R version 4.1.3 (2022-03-10) -- "One Push-Up"

# Information on files
This project is structured as a Rproject (MIX-ENABLE.Rproj), openable with RStudio.

## Main file (in script directory)
1/ positive-deviance_analysis.rmd = run it and it will proceed to the positive deviance analysis

## Other files
- data*: raw data + computed data from the other Rproject (https://forgemia.inra.fr/mix-enable/mix-enable_indicators)
    - data-sample/*: shuffled and sampled data => NOT REAL, as a reproductible example only. Real data can be found: https://data.inrae.fr/dataset.xhtml?persistentId=doi:10.15454/AKEO5G
- scripts/*: all R and RMD scripts
